def factors(number):
    # ==============
    # Your code here
    factorials = set(
        factor for i in range(2, number - 1) if number % i == 0
        for factor in (i,)
    )
    if len(factorials) == 0:

        return   '{} is a prime number'.format(number)
    else:
        return list(factorials)
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print "13 is a prime number"
