def factors(number):
    # ==============
    # Your code here
        factorials = set(
            factor for i in range(2 , number -1) if number % i == 0
            for factor in (i,)
    )
        return list(factorials)
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13))
